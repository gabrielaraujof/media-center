# Self-Hosted ARM-Based Media Center

A set of docker swarm services self-hosted on a ARM-based board to provide a self-manageable local media center.

## Requirements

- Docker

## Architecture

We use stacks for each independent service. Which service with one or more tasks. They are:

- Plex
- Traefik network
- Traefik
- Portainer
- Shepherd
- Blackbeard
  * Sonarr
  * Radarr
  * Jackett
  * Transmission
- Dnsmasq (currently not working on swarm)

## Credit

The project is based on other projects and initiatives, as:

- [Docker home media server](https://www.smarthomebeginner.com/docker-home-media-server-2018-basic/)
- [Geek Cookbook](https://geek-cookbook.funkypenguin.co.nz/)


